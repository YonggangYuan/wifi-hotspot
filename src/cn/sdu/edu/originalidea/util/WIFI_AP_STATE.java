
package cn.sdu.edu.originalidea.util;

/**
 * 描述wifi AP 的状态
 * @author Yonggang Yuan
 *
 */

public enum WIFI_AP_STATE {

    /* wifi AP 正在关闭中 */
    WIFI_AP_STATE_DISABLING,

    /* wifi AP 不可用 */
    WIFI_AP_STATE_DISABLED,

    /* wifi AP 正在打开中 */
    WIFI_AP_STATE_ENABLING,

    /* wifi AP 可用 */
    WIFI_AP_STATE_ENABLED,

    /* wifi AP 故障 */
    WIFI_AP_STATE_FAILED
}