package cn.sdu.edu.originalidea;

import java.util.ArrayList;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import cn.sdu.edu.originalidea.util.ClientScanResult;
import cn.sdu.edu.originalidea.util.WifiApManager;

/**
 * 
 * @author Yonggang Yuan
 *
 */

public class MainActivity extends Activity {
    TextView textView1;
    WifiApManager wifiApManager;
    String url = "http://www.baidu.com/s";
    String paramName = "wd";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        textView1 = (TextView) findViewById(R.id.textView1);
        wifiApManager = new WifiApManager(this);
        scan(url);
    }

    private void scan(String url) {
        ArrayList<ClientScanResult> clients = wifiApManager.getClientList(false);

        textView1.setText("WifiApState: " + wifiApManager.getWifiApState() + "\n\n");

        textView1.append("Clients: \n");
        for (ClientScanResult clientScanResult : clients) {
            textView1.append("####################\n");
            textView1.append("IpAddr: " + clientScanResult.getIpAddr() + "\n");
            textView1.append("Device: " + clientScanResult.getDevice() + "\n");
            textView1.append("HWAddr: " + clientScanResult.getHWAddr() + "\n");
            textView1.append("isReachable: " + clientScanResult.isReachable() + "\n");

            textView1.append("Post HWAddr to " + url);
            String response = wifiApManager.sendGet(url, paramName + "=" + clientScanResult.getHWAddr());
            if(response == null || "".equals(response)) {
                textView1.append("Errors appeared when post to " + url + " or when get data from that site");
                continue;
            }
            response = response.length()>50 ? response.substring(0, 50) : response;
            textView1.append("Reponse(May be not all, since too long text): \n" + response);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Get Clients");
        menu.add(0, 1, 0, "Open AP");
        menu.add(0, 2, 0, "Close AP");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
        case 0:
            scan(url);
            break;
        case 1:
            wifiApManager.setWifiApEnabled(null, true);
            break;
        case 2:
            wifiApManager.setWifiApEnabled(null, false);
            break;
        }

        return super.onMenuItemSelected(featureId, item);
    }
}